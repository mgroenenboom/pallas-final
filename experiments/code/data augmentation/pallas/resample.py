#!/usr/bin/env python
# coding: utf-8

# # Resample samples
# 
# ---
# 
# Defines functions for resampling the dataset
# 
# Defines the following functions:
# - list_labels - List all labels in the given folder
# - resample_all - Resample all samples in a given folder
# - resample_folder - Resample all samples of a given label
# - resample_file - Resample a single file

from tqdm.auto import tqdm

import numpy as np
import pandas as pd

import glob
import os

from audio import load_file, write_file

def split(folder, proportions, seed):
    """Splits the samples in the given folder into different sets.
    
    Parameters
    ----------
    folder : str, optional
        The folder containing labelled wave files (default is sample_folder from config.py)
    proportions : obj(set_name: set_amount), optional
        The sets and proportions to use (default is split_proportions from config.py)
    seed: int, optional
        Random seed to use (default is default_seed from config.py)
    """
    samples_split = {}
    input_folders = os.listdir(folder)
    sets = list(proportions.keys())
    
    for class_folder in input_folders:
        samples_split[class_folder] = {}
        input_files = glob.glob(f'{folder}/{class_folder}/*.wav')
        
        # Shuffle the input file array
        rng = np.random.default_rng(seed=seed)
        rng.shuffle(input_files)
        
        # Split the shuffled input file array
        split_indices = [int(value * len(input_files)) for value in proportions.values()]
        split_indices = [sum(split_indices[:i+1]) for i in range(len(split_indices))]
        folder_split = np.split(input_files, split_indices)
        for i in range(len(sets)):
            for file in folder_split[i]:
                samples_split[class_folder][os.path.basename(file)] = sets[i]
        for file in folder_split[-1]:
            samples_split[class_folder][os.path.basename(file)] = sets[0]
    return samples_split

def format_volume(volume):
    """Format the volume such that 1. the sign is always visible and 2. 0 is an empty string
    
    Parameters
    ----------
    volume : int
        The volume to format
    """
    if volume == 0:
        return ""
    else:
        return f'{volume:+g}db'

def list_labels(df_samples, df_samples_train=None, df_samples_test=None):
    """Lists all labels in the dataset.
    
    Parameters
    ----------
    df_samples : pandas.DataFrame
        A pandas.DataFrame of all samples in the dataset
    df_samples_train : pandas.DataFrame
        A pandas.DataFrame of all train samples in the dataset
    df_samples_test : pandas.DataFrame
        A pandas.DataFrame of all test samples in the dataset
    Returns
    -------
    pandas.DataFrame
        A pandas.DataFrame of all labels in the dataset with the amount of samples per set
    """
    if df_samples_train is None:
        df_samples_train = df_samples[df_samples.set == 'train']
    if df_samples_test is None:
        df_samples_test = df_samples[(df_samples.set == 'test') & (df_samples.volume == 0)]
    
    label_rows = []
    for label in df_samples['label'].unique():
        train = sum(df_samples_train['label'] == label)
        test = sum(df_samples_test['label'] == label)
        validate = 0
        label_rows.append([label, train + test + validate, train, test, validate])
    return pd.DataFrame(label_rows, columns=["label", "amount of samples", "train", "test", "val"])

def resample_all(dataset_folder, output_folder, set_split, config):
    """Resamples all wav files in the given labelled samples folder.
    
    Parameters
    ----------
    dataset_folder : str
        The folder containing labelled wave files in subfolders for each class
    output_folder : str
        The folder that resampled wave files will be written to
    set_split : obj
        Object describing the set to use for the given file
        Structured as set_split[folder][file] = set
    volumes : list(int), optional
        The volumes to resample to (default is [0])
    
    Returns
    -------
    pandas.DataFrame
        A DataFrame of all samples in the dataset
    """
    os.makedirs(output_folder, exist_ok=True)
    input_folders = os.listdir(dataset_folder)
    samples = []
    volumes = [0]
    try:
        volumes = config.augmentation.combinations.db_offsets
    except:
        pass
    
    set_volumes = {'train': volumes, 'test': [0]}
    
    # Look through all class folders
    for class_folder in tqdm(input_folders, desc=f'Resampling samples'):
        input_files = glob.glob(f'{dataset_folder}/{class_folder}/*.wav')
        
        # Resample each file in directory
        for file in tqdm(input_files, leave=False):
            filename = os.path.basename(file)
            sample_set = set_split[class_folder][filename]
            for volume in set_volumes[sample_set]:
                
                # Generate paths
                input_file = f'{dataset_folder}/{class_folder}/{filename}'
                out_folder = f'{output_folder}/{sample_set}/{class_folder}{format_volume(volume)}'
                new_filename = filename
                output_file = f'{out_folder}/{new_filename}'
                os.makedirs(out_folder, exist_ok=True)
                
                samples.append([sample_set, class_folder, out_folder, new_filename, input_file, volume])
                
                # Skip if file already exists
                if os.path.isfile(output_file):
                    continue
                # Else if it isn't a file but it does exist, throw error
                elif os.path.exists(output_file):
                    raise IsADirectoryError(f"Output {output_file} already exists but it isn't a file")
                
                in_sr, in_frames = load_file(input_file, sr=config.augmentation.rate, shuffle=False)
                
                # Change volume
                if volume != 0:
                    gain = 10.0 ** (volume / 10.0)
                    in_frames = (in_frames * gain).astype(np.int16)
                
                write_file(output_file, in_frames, in_sr)
    
    df = pd.DataFrame(samples, columns=['set', 'label', 'folder', 'file', 'source_file', 'volume'])
    df.index.name = 'index'
    return df
