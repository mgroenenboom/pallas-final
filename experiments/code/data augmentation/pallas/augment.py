#!/usr/bin/env python
# coding: utf-8

# # Data augmentation
# 
# ---
# 
# Defines functions for augmenting the dataset
# 
# Defines the following functions:
# - is_valid_combination - Used to filter only valid combinations of label-volume pairs
# - create_combinations - Creates combinations of label-volume pairs of the dataset
# - combine - Combine samples of a label-volume pair
# - combine_samples - Combine two samples to get a new augmented sample

import os
from itertools import permutations, product, cycle

from tqdm.auto import tqdm, trange
import numpy as np
import pandas as pd
import pathlib

from audio import load_file, write_file, combine_files, add_noise

def format_volume(volume):
    """Format the volume such that 1. the sign is always visible and 2. 0 is an empty string
    
    Parameters
    ----------
    volume : int
        The volume to format
    """
    if volume == 0:
        return ""
    else:
        return f'{volume:+g}db'

def is_valid_combination_filter(allow_same_class=False):
    """Test whether a combination is a valid combination
    
    A combination is considered valid if the labels are different and the volume of the first is larger than the second.
    Furthermore, 'silence' labels cannot be used to combine samples.
    
    Parameters
    ----------
    combination : tuple(tuple(label, volume), tuple(label, volume))
        A combination of label-volume pairs
    
    Returns
    -------
    bool
        Whether the combination is valid or not
    """
    def is_valid_combination(combination):
        label1 = combination[0]
        label2 = combination[1]

        # Case: Volume of label2 is larger than volume of label1
        if label1[1] < label2[1]:
            return False
        # Case: They have the same volume
        if label1[1] == label2[1]:
            return False
        # Case: Any of the labels is considered silence
        if 'silence' in label1[0] or 'silence' in label2[0]:
            return False
        # Case: They have the same label
        if label1[0] == label2[0]:
            return allow_same_class
        # All other cases:
        return True
    return is_valid_combination

def create_combinations(df_samples, augmented_folder, config, seed):
    """Creates combinations of samples of the provided dataset
    
    Parameters
    ----------
    df_samples : DataFrame
        A dataframe containing all samples to use for combining
    seed: int, optional
        Seed to use for random operations (default is default_seed from config.py)
    
    Returns
    -------
    pandas.DataFrame
        A DataFrame of all sample combinations.
    """
    labels = df_samples['label'].unique()
    volumes = df_samples['volume'].unique()
    all_combinations = permutations(product(labels, volumes), 2)
    os.makedirs(augmented_folder, exist_ok=True)
    filtered_combinations = list(filter(is_valid_combination_filter(allow_same_class=True), all_combinations))
    combined_samples = []
    for combination in tqdm(filtered_combinations, desc='Combining samples'):
        combine(combination, config, df_samples, combined_samples, seed, augmented_folder)
    return pd.DataFrame(combined_samples, columns=['set', 'folder1', 'file1', 'label_1', 'volume_1', 'folder2', 'file2', 'label_2', 'volume_2', 'folder', 'file'])

def combine(combination, config, df_samples, new_samples, seed, augmented_folder):
    """Combine samples according to the given combination
    
    Parameters
    ----------
    combination : tuple(tuple(label, volume), tuple(label, volume))
        A combination of label-volume pairs
    df_samples : DataFrame
        A dataframe containing all samples in the dataset
    new_samples : list([set, label1, label2, volume_1, volume_2, folder, filename])
        Resampled files are added to this list
    seed : int
        Seed to use for random operations
    """
    selection_0 = df_samples[(df_samples.label == combination[0][0]) & (df_samples.volume == combination[0][1])]
    selection_1 = df_samples[(df_samples.label == combination[1][0]) & (df_samples.volume == combination[1][1])]
    combinations = list(product(selection_0.to_records()['index'], selection_1.to_records()['index']))
    rng = np.random.default_rng(seed=seed)
    rng.shuffle(combinations)
    combinations = combinations[0:int(config.augmentation.combinations.amount)]
    
    output_folder = f'{augmented_folder}/{combination[0][0]}{format_volume(combination[0][1])}+{combination[1][0]}{format_volume(combination[1][1])}'
    
    for combination in tqdm(combinations, leave=False):
        combine_samples(selection_0.loc[[combination[0]]], selection_1.loc[[combination[1]]], output_folder, new_samples, config)

def combine_samples(sample1, sample2, output_folder, new_samples, config):
    """Combine two samples to produce a new augmented sample
    
    Parameters
    ----------
    sample1 : DataFrame
        A DataFrame containing the first sample to use
    sample2 : DataFrame
        A DataFrame containing the second sample to use
    output_folder : str
        Folder to write the new sample to
    new_samples : list([set, label1, label2, volume_1, volume_2, folder, filename])
        Resampled files are added to this list
    save : bool, optional
        Whether to save combined samples to drive.
        If False, samples are not written to drive and they have to be read and combined during feature extraction.
    """
    sample1_index = sample1.index.values[0]
    sample2_index = sample2.index.values[0]
    sample1 = sample1.to_dict('index')[sample1_index]
    sample2 = sample2.to_dict('index')[sample2_index]
    
    in1_file = f'{sample1["folder"]}/{sample1["file"]}'
    in2_file = f'{sample2["folder"]}/{sample2["file"]}'
    output_file = f'{sample1_index}-{sample2_index}.wav'
    
    new_samples.append([sample1['set'], sample1["folder"], sample1["file"], sample1['label'], sample1['volume'], sample2["folder"], sample2["file"], sample2['label'], sample2['volume'], output_folder, output_file])
    
    out_file = f'{output_folder}/{output_file}'
    
    # Skip if file already exists
    if os.path.isfile(out_file):
        return
    # Else if it isn't a file but it does exist, throw error
    elif os.path.exists(out_file):
        raise IsADirectoryError(f"Output {out_file} already exists but it isn't a file")
    
    if config.augmentation.combinations.save_combinations:
        os.makedirs(output_folder, exist_ok=True)
        out_sr, out_frames = combine_files(in1_file, in2_file, config.augmentation.rate, shuffle=True)
        write_file(out_file, out_frames, out_sr)

def rebalance_classes(df_samples, augmented_folder, config):
    # Determine target amount
    target_amount = config.augmentation.rebalance.amount
    if target_amount is None:
        max_amount = 0
        for label in df_samples['label'].unique():
            count = len(df_samples[df_samples['label'] == label])
            max_amount = max(max_amount, count)
    
    # Determine type or rebalance and snr lambda
    if config.augmentation.rebalance.type == 'noise':
        if config.augmentation.rebalance.min == config.augmentation.rebalance.max:
            snr = lambda: config.augmentation.rebalance.min
        else:
            snr = lambda: np.random.default_rng().integers(config.augmentation.rebalance.min, config.augmentation.rebalance.max)
    
    df_parts = []
    for label in tqdm(df_samples.label.unique(), desc=f'Rebalancing ({config.augmentation.rebalance.type})'):
        df_label = df_samples[df_samples['label'] == label]
        aug_samples = []
        iter_samples = cycle(df_label.itertuples())
        
        first = None
        cycles = 0
        output_folder = f'{augmented_folder}/{label}'
        os.makedirs(output_folder, exist_ok=True)
        
        label_amount = max(target_amount, len(df_label))
        if hasattr(config.augmentation.rebalance, 'max_amount'):
            label_amount = min(label_amount, config.augmentation.rebalance.max_amount)
        
        for i in trange(label_amount, leave=False):
            sample = next(iter_samples)
            input_file = f'{sample.folder}/{sample.file}'
            if first is None:
                first = sample
            elif first == sample:
                cycles += 1
            output_file = f'{sample.file}_{cycles}.wav'
            
            # Skip if file already exists
            if os.path.isfile(f'{output_folder}/{output_file}'):
                continue
            # Else if it isn't a file but it does exist, throw error
            elif os.path.exists(output_file):
                raise IsADirectoryError(f"Output {output_file} already exists but it isn't a file")
            
            # Load file and add noise if doing noise rebalance
            sr, frames = load_file(input_file, sr=config.augmentation.rate, shuffle=True)
            if config.augmentation.rebalance.type == 'noise':
                sample_snr = snr()
                aug_samples += [[sample.set, sample.label, output_folder, output_file, input_file, sample.volume, sample_snr]]
                frames = add_noise(frames, sample_snr, color='pink')
            else:
                aug_samples += [[sample.set, sample.label, output_folder, output_file, input_file, sample.volume]]
            
            write_file(f'{output_folder}/{output_file}', frames, frame_rate=sr)
        if config.augmentation.rebalance.type == 'noise':
            df_parts += [pd.DataFrame(aug_samples, columns=['set', 'label', 'folder', 'file', 'source_file', 'volume', 'snr'])]
        else:
            df_parts += [pd.DataFrame(aug_samples, columns=['set', 'label', 'folder', 'file', 'source_file', 'volume'])]
    return pd.concat(df_parts, ignore_index=True)
