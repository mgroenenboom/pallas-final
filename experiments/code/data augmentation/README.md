# Data augmentation code

---

This directory contains all code necessary to run the data augmentation techniques that were compared in my thesis.

Ensure that next to the code folder is a data folder containing the dataset and label files.

The initial file tree should look like this:

- code
  - data augmentation
  - edge_ai
- data
  - data augmentation
    - dataset
      - [one folder for each class]
      - *labels.csv*
    - vggish
      - *vggish_audioset_weights_without_fc2.h5*
      - *vggish_audioset_weights.h5*
      - *vggish_model.ckpt*
      - *vggish_pca_params.npz*
    - *01_input_selected_classes.csv*
    - *04_input_selected_classes.csv*

## How to run

The files require Jupyter to run. Once Jupyter and all dependencies are installed, run the files in the following order:

- 1-Data-augmentation.ipynb

    Run all data augmentation techniques

- 2-Embed-all.ipynb

    Embed all augmented datasets with VGGish

- 3-Train-all.ipynb

    Train a Serval model for each augmented dataset

- 4a-Generate-test-samples.ipynb

    Generate double-labelled test samples

- 4b-Test-all.ipynb

    Predict the results for each data augmentation technique with the single-labelled and double-labelled test samples

- 4c-Show-results.ipynb

    Show the results for each data augmentation technique
