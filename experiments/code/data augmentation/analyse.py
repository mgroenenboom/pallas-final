import numpy as np
import pandas as pd
from tqdm.auto import tqdm
import re

def latex_report_classes(df_samples, df_class_labels):
    folder_to_label_dict = {}
    for index, row in df_class_labels.iterrows():
        folder_to_label_dict[row.folder_name] = row.label
    
    combinations_count = np.zeros((11, 9), dtype=np.int32)
    
    for file in tqdm(df_samples['filepath'].unique()):
        file_folder = file.split('/')[-4:-1]
        # File is in the eval set: ignore
        if file_folder[0] == 'eval' or file_folder[1] == 'test':
            continue

        # File is in the train set with a single label (these have an extra subfolder for the dampening)
        if file_folder[0] == 'train':
            label = folder_to_label_dict[file_folder[2]]
            primary_label = label
            secondary_label = label
        # File is in the train set with multiple labels
        else:
            label_volume = file_folder[2]
            try:
                matches = re.findall('^combine_(\d+)_(\d+)_(\d+)db_(\d+)db', label_volume)
                # If Pallas sample:
                if len(matches) == 0:
                    # If single label Pallas sample:
                    if file_folder[2] in folder_to_label_dict:
                        label = folder_to_label_dict[file_folder[2]]
                        label1 = label
                        label2 = label
                        vol1 = 0
                        vol2 = 0
                    # If multi-label Pallas sample:
                    else:
                        label_volume = file_folder[2].split('+')
                        # print(label_volume)
                        label_volume = [l.split('-') for l in label_volume]
                        # print(label_volume)
                        if len(label_volume[0]) == 1:
                            label1 = folder_to_label_dict[label_volume[0][0]]
                            vol1 = 0
                        else:
                            label1 = folder_to_label_dict[label_volume[0][0]]
                            vol1 = label_volume[0][1].replace('db', '')
                        if len(label_volume[1]) == 1:
                            label2 = folder_to_label_dict[label_volume[1][0]]
                            vol2 = 0
                        else:
                            label2 = folder_to_label_dict[label_volume[1][0]]
                            vol2 = label_volume[1][1].replace('db', '')
                # If Serval sample:
                else:
                    [(label1, label2, vol1, vol2)] = matches
            except:
                print(file_folder, matches)
                raise
            if int(vol1) > int(vol2):
                primary_label = int(label2)
                secondary_label = int(label1)
            else:
                primary_label = int(label1)
                secondary_label = int(label2)

        # Add one to the cell for this combination
        combinations_count[secondary_label-1, primary_label-1] += 1
        # Add one to the cell for total primary label or no combination samples
        combinations_count[9, primary_label-1] += 1
        # Add one to the cell for total label samples
        combinations_count[10, primary_label-1] += 1
        # Add one to the cell for total label samples for the secondary if it is a different label
        if primary_label != secondary_label:
            combinations_count[10, secondary_label-1] += 1

    labels = [f"{' '.join(row.folder_name.split('_')[1:]).capitalize()} ({row.label})" for row in df_class_labels.itertuples()] + ['Total primary', 'Total']
    rules = {0: '\\midrule', len(df_class_labels): '\\midrule'}

    rows = ['\\toprule']
    rows += [' & '.join([f"{' '*len(max(labels, key=len)) if elem == 0 else elem:<5}" for elem in range(len(combinations_count)-1)]) + '\\\\']
    for idx, row in enumerate(combinations_count):
        if idx in rules:
            rows += [rules[idx]]
        rows += [" & ".join([f"{labels[idx]:<{len(max(labels, key=len))}}"] + [f"{'' if elem == 0 else elem:<5}" for elem in row]) + '\\\\']
    rows += ['\\bottomrule']
    
    return rows