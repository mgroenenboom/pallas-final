import audioop
from scipy.io import wavfile
import numpy as np

def load_file(wavefile, sr, shuffle):
    """Loads the given wave file and returns the contents, converted to be vggish compatible, and its parameters.
    
    Parameters
    ----------
    wavefile : str
        The file to load
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    wave_sr, wave_frames = wavfile.read(wavefile)
    wave_sr, wave_frames = ensure_properties(wave_sr, wave_frames, sr)
    if shuffle:
        wave_frames = shuffle_frames(wave_frames)
    return wave_sr, wave_frames

def to_float(sr, frames):
    """Converts int16 frames to floating point [-1, 1]
    
    Parameters
    ----------
    frames
        The audio frames
    sr
        The sample rate of the frames
    
    Returns
    -------
    float[]
        The floating point audio frames
    sr
        The sample rate
    """
    return sr, frames / 2**15

def ensure_properties(sr, frames, target_rate, left=1, right=1):
    """Convert the frames to the chosen properties
    
    Parameters
    ----------
    sample_rate: int
        The audio sample rate
    frames : numpy.ndarray
        An array representing an audio wave
    target_rate : int
        The target framerate
    left : float, optional
        The proportion of the left channel when converting stereo to mono (default is 1)
    right : float, optional
        The proportion of the right channel when converting stereo to mono (default is 1)
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    return ensure_rate(sr, ensure_mono(frames, left, right), target_rate)

def ensure_mono(frames, left=1, right=1):
    """Convert the frames to be mono, if stereo
    
    Parameters
    ----------
    frames : numpy.ndarray
        An array representing an audio wave
    left : float, optional
        The proportion of the left channel when converting stereo to mono (default is 1)
    right : float, optional
        The proportion of the right channel when converting stereo to mono (default is 1)
    
    Returns
    -------
    numpy.ndarray
        The audio wave frames
    """
    if frames.ndim != 1:
        return frames[:, 0] * left + frames[:, 1] * right
    return frames

def ensure_rate(sample_rate, frames, target_rate):
    """Convert the frames to be the given target_rate, if necessary
    
    Parameters
    ----------
    sample_rate : int
        The audio frame rate
    frames : numpy.ndarray
        An array representing an audio wave
    target_rate : int, optional
        The target framerate (default is the vggish compatible value 16KHz)
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    if sample_rate == target_rate or target_rate is None:
        return sample_rate, frames
    frames_bytes = frames.tobytes()
    frames_resampled = audioop.ratecv(
        frames_bytes, 2, 1, sample_rate, target_rate, None
    )[0]
    return target_rate, np.frombuffer(frames_resampled, dtype=np.int16)

def shuffle_frames(frames):
    i = np.random.randint(frames.size)
    shuffled_frames = np.append(frames[i:],frames[:i])
    return shuffled_frames

def combine_files(in1, in2, sr, shuffle):
    """Combine the given input wave files and return the combined frames
    
    Parameters
    ----------
    in1 : str
        String pointing to a wave file
    in2 : str
        String pointing to a wave file
    
    Returns
    -------
    int
        The sample rate
    numpy.ndarray
        The audio wave frames
    """
    sr1, frames1 = load_file(in1, sr, shuffle)
    sr2, frames2 = load_file(in2, sr, shuffle)
    assert sr1 == sr2
    length = min(len(frames1), len(frames2))
    return sr1, frames1[:length] + frames2[:length]

def write_file(file, frames, frame_rate):
    """Write a wave file
    
    Parameters
    ----------
    file : str
        The destination wave file
    frames : numpy.ndarray
        An array representing an audio wave
    frame_rate : int, optional
        The framerate of the target file (default is the vggish rate, 16 KHz)
    """
    assert frames.dtype == np.int16
    wavfile.write(file, frame_rate, frames)

def add_noise(frames, snr, color):
    """
    Parameters
    ----------
    frames : numpy.ndarray
        An array representing an audio wave
    snr : float
        The snr of the result signal.
    color : str, optional
        The type of noise to add. Default is white noise ("white").
        Currently only accepts "white" and "pink"
    
    Returns
    -------
    numpy.ndarray
        The audio wave frames
    """
    float_frames = frames / 2**16
    frames_power = float_frames ** 2

    # Calculate signal power and convert to dB
    mean_power = np.mean(frames_power)
    mean_db = 10 * np.log10(mean_power)
    # Calculate noise dB then convert to power
    noise_mean_db = mean_db - snr
    noise_mean_power = 10 ** (noise_mean_db / 10)
    noise_mean = 0
    
    # Generate the noise
    noise = np.random.normal(noise_mean, np.sqrt(noise_mean_power), float_frames.shape)
    
    # Change noise color if necessary:
    # Code based on https://stackoverflow.com/questions/67085963/generate-colors-of-noise-in-python
    if color != "white":
        noise = np.fft.rfft(noise)
        S = np.fft.rfftfreq(len(float_frames))
        
        if color == "blue":
            S = np.sqrt(S)
        elif color == "violet":
            pass
        elif color == "brownian":
            S = 1/np.where(S==0, float('inf'), S)
        elif color == "pink":
            S = 1/np.where(S==0, float('inf'), np.sqrt(S))
        else:
            raise ValueException("Invalid noise color entered: ", color)
        S /= np.sqrt(np.mean(S**2))
        noise *= S
        noise = np.fft.irfft(noise)
    
    # Noise up the original signal
    result = frames[:len(noise)] + (noise * 2**16).astype(np.int16)
    return result
