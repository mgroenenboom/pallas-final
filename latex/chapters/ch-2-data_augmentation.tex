\chapter{Data augmentation}
\label{chp:da}

    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig amt of samples.pdf}
        \caption{The amount of samples in the dataset for each class. The imbalance between classes can clearly be seen.}
        \label{fig:da:samples_imbalance}
    \end{figure}

    \begin{table}[t]
        \centering
        \input{chapters/images/4. DA/tab samples samplerate}
        \caption{The amount of samples per sample rate in the dataset for each class.}
        \label{tab:da:samples_samplerate}
    \end{table}

    \begin{table}[t]
        \centering
        \input{chapters/images/4. DA/tab datasets}
        \caption{Different audio datasets with their amount of samples and amount of classes listed.}
        \label{tab:da:datasets}
    \end{table}

    The most obvious way to train a model is through supervised learning with a labelled dataset. However training a model for \acrfull{aed} is a challenge due to the need for large amounts of training data and the high cost of labeling such data.

    As presented in \autoref{sec:SoA_DA}, data augmentation is one possible solution. Also, Serval already employs data augmentation, so exploring the different options here feels like a promising option. Furthermore data augmentation can be used as a way to eliminate or mitigate class imbalances. Serval is able to recognize multiple classes at the same time. If a sample contains both car noises and a gunshot, Serval is able to classify both of these sounds at the same time. This requires a dataset with multi-label samples, samples with sounds from multiple classes occurring at the same time or shortly after each other. However, data augmentation can be applied to a dataset with single-label samples to generate multi-label samples, which is necessary to train a model to do this.

    As we can see in \autoref{fig:da:samples_imbalance}, Serval's dataset suffers from heavy class imbalance: for each motor sample there are almost 17 gunshot samples. Furthermore with 3413 samples the amount of samples seems to be rather small for a modern dataset, considering the amount of samples in commonly used or even dated audio datasets as seen in \autoref{tab:da:datasets}. Furthermore, not all samples are in the same samplerate. This is evident in \autoref{tab:da:samples_samplerate}. While this should not be a problem for VGGish as it uses 16KHz input, OpenL3 uses 48KHz input which may compromise the improvements gained from using the newer embedding.

    This chapter explores multiple possible data augmentation techniques. \Autoref{sec:da:serval} details the data augmentation technique that Serval uses, while \autoref{sec:da:imp_combinations} explores an improvement upon this method. \Autoref{sec:da:noise_augmentation} explores a method using noise to differentiate samples and finally \autoref{sec:da:hybrid} details a method combining multiple of the mentioned methods to achieve a hybrid type of augmentation.

    \clearpage\warn{clearpage check}

\section{Serval cross-label sample combining}
\label{sec:da:serval}
    
    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig serval process.pdf}
        \caption{Serval cross-label sample combining. First samples are resampled from their original frequency to 16KHz. These samples are then dampened by 6 and 12 dB. Then they are split 50\%/50\% in a train and a test set. Finally the training samples are combined to generate a larger and more balanced training set. For each combination a sample is combined with a softer sample of a different class.}
        \label{fig:da:serval_process}
    \end{figure}
    
    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig serval detail.pdf}
        \caption{Serval cross-label sample combining detail with only two classes. For each class, dampened samples are created. The original and dampened samples are combined with samples from other classes. The primary class is the class of the loudest sample used for the combination.}
        \label{fig:da:serval_detail}
    \end{figure}
    
    \begin{table}[t]
        \centering
        \begin{adjustbox}{width=0.955\textwidth}
        \input{chapters/images/4. DA/tab samples serval}
        \end{adjustbox}
        \caption{
            The amount of samples per class combination after cross-label sample combination and the total amount of samples per class.
            Horizontally are the primary labels, vertically the secondary labels.
            The total primary counts all samples where the primary class is the given class.
            The total either counts all samples where the given class is present--either as primary or as secondary label.
        }
        \label{tab:da:serval_samples}
    \end{table}
    
    A particular data augmentation technique that is interesting to us is is the technique employed by Serval, which involves combining samples with samples with a different label, yielding new multi-label samples. This technique will be referred to as Serval cross-label sample combining in the remainder of this thesis.
    
    \autoref{fig:da:serval_process} shows the steps in this data augmentation and \autoref{fig:da:serval_detail} illustrates the data augmentation process in detail with two example classes.
    If we take only the gunshots and mopeds classes, first all gunshot samples are time-shifted: they are cut in half at a random place and then wrapped around. Do note that this may cut in the middle of the relevant event, however this may happen in real time recordings as well and may improve the model's ability to correctly recognize in such cases.
    Then they are resampled to 16000 kHz (the frequency used by the embedding VGGish) and copies dampened by 6 dB and by 12 dB are created.
    These dampened samples will be referred to as gunshots -6dB and gunshots -12dB from now on. This is done for all classes. Half of these samples are reserved for validation.
    
    Subsequently, a number of samples are created by combining gunshot samples with softer moped samples. This results in three new multi-label sample sets: gunshots combined with mopeds -6dB, gunshots combined with mopeds -12dB, and gunshots -6dB combined with mopeds -12dB. These samples have gunshots as their primary label and mopeds as their secondary label, as the gunshot sample was louder than the moped sample. No other combinations are made as the moped samples may not be louder than the gunshot samples due to the gunshot samples being earlier in the order of classes. This seemingly arbitrary decision will be addressed in \autoref{sec:da:imp_combinations}. For each possible combination allowed by these rules, by default 500 samples are created. Finally, all dampened single-class samples are also included in the dataset.

    Why does Serval use this data augmentation? First of all, this is an effective way to rebalance the classes. As previously seen in \autoref{fig:da:samples_imbalance}, there is a significant imbalance between the amount of samples of classes. The augmented dataset in \autoref{tab:da:serval_samples} demonstrates that this imbalance has been completely eliminated, and the number of samples has been significantly increased. Additionally, this data augmentation technique allows the model to be trained for multi-label classification without requiring a multi-label annotated dataset.

    It can also be observed that the single-label samples on the diagonal are still highly imbalanced and few compared to the multi-class samples. This may compromise the model's ability to accurately label classes that contain only a single sample. Moreover, combinations above the diagonal are omitted. These are samples where the 'later' occurring class has a higher volume. This results in the moped alarms class having only dampened samples in the augmented dataset, which may compromise the model's ability to recognize this class.

    Additionally, dampening is done before doing a train test split. This results in samples and their dampened variants being in both of the sets. For example if we take a random sample, the original volume sample may be in the train set, with the dampened variant in the test set. These samples may be too similar causing the test metrics to be unreliable by making it harder to detect overfitting.

    % \clearpage\warn{clearpage check}

\section{Improved sample combining}
\label{sec:da:imp_combinations}
    
    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig improved combinations process.pdf}
        \caption{Improved sample combining. Similar to Serval cross-label sample combining, however some of the flaws were fixed. First of all the train/test split was done before the dampening. Furthermore no uncombined samples are added, instead single-label samples are combined samples as well}
        \label{fig:da:improved_combinations_process}
    \end{figure}
    
    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig improved combinations detail.pdf}
        \caption{Improved sample combining detail with only two classes. For each class, dampened samples are created. Highlighted are the sample combinations shown in \autoref{fig:da:serval_detail}. Combinations where mopeds are louder than gunshots are now additionally created, as well as single-label combinations of two different volume gunshot samples}
        \label{fig:da:improved_combinations_detail}
    \end{figure}
    
    \begin{table}[t]
        \centering
        \begin{adjustbox}{width=\linewidth}
        \input{chapters/images/4. DA/tab samples serval improved}
        \end{adjustbox}
        \caption{The amount of samples per class combination after improved sample combination and the total amount of samples per class.}
        \label{tab:da:improved_combinations_samples}
    \end{table}

    The most straightforward way to improve the data augmentation technique mentioned in \autoref{sec:da:serval}, is to allow the omitted combinations mentioned in the same section. This is illustrated in \autoref{fig:da:improved_combinations_detail}.
    First the samples on the diagonal, which are single-class samples, can be combined samples as well; Takahashi et al. \cite{takahashi_interspeech} proposed that two same-class samples can also be combined to create a new sample, thus balancing the classes better.
    Second, the samples above the diagonal are omitted in Serval's data augmentation. This causes the augmented dataset to remain imbalanced. This also can be improved to yield a more balanced augmented dataset.

    Concretely this means that while Serval would not generate samples from gunshots -12dB and mopeds -6dB, because here the moped samples are louder than the gunshots samples, this improved method fixed this problem and also generates samples of gunshots -12dB combined with mopeds -6dB.
    Applying this change yields us the combinations table shown in \autoref{tab:da:improved_combinations_samples}.
    This method will be referred to as Improved cross-label sample combining.

    Because this effectively means we get over 2 times as many samples, the amount of samples per possible combination is halved from 500 to 250 to get approximately the same total amount of samples as with Serval cross-label sample combining.

    A second change is the shift from a 50\%/50\% train/test split to a 80\%/20\% train/test split, as illustrated in \autoref{fig:da:improved_combinations_process}. This is a more standard balance between the train and test sets, and it should allow of a better model at the cost of less reliable accuracy metrics. Because the Serval training code does not use a validation set and because of the size of the dataset, I opted to do a train/test split just like the Serval cross-label sample combining technique. An important difference however is that the train/test split is done before any dampening. This ensures that a sample does not have similar (dampened or original) samples in the other set, and that the test samples are unmodified instead of potentially dampened.

    As shown in the new table, all classes now have an equal amount of primary-labelled samples. This eliminates the possible imbalance due to classes lacking `easier' samples. Furthermore, this modified data augmentation technique now also generates single-label combined samples (samples gained by combining two samples with the same class), while Serval only generates multi-label combined samples (samples gained by combining two samples with different classes). This eliminates the class imbalance in single-class samples.

    % \clearpage\warn{clearpage check}

\section{Noise sample augmentation}
\label{sec:da:noise_augmentation}

    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig noise augment process.pdf}
        \caption{Noise sample augmentation. A layer of pink noise is added to the resampled and time shifted samples. This only happens to train samples. Because no combinations are being made, the dampening is not necessary and is skipped.}
        \label{fig:da:noise_process}
    \end{figure}

    The most basic rebalancing technique is to simply train with samples from underrepresented classes multiple times. This rebalances the dataset but could introduce major \gls{overfitting}\todo{Overfitting uitleggen} in the underrepresented classes. One way to avoid overfitting is to add a layer of noise to the samples as proposed by Salamon and Bello \cite{cnn_data_augmentation_esc}. Depending on the level of noise, this may differentiate the samples sufficiently to eliminate the overfitting. While Salamon and Bello use background noises such as street and park ambient noise, I opted to use generated pink noise, which, according to Szendro et al. can be regarded as the most natural type of noise\cite{bio-response-noise}. It is also approximately the sound generated by waterfalls. As shown in \autoref{fig:da:noise_process}, no dampening is done as these were necessary for the combinations.

    The main advantage of using this technique is that it is fast and easy to implement. The samples are also easier to train on and are more similar to the samples in the test set. This should speed up training compared to the methods mentioned earlier. Additionally training the model to cope with noise should help it to recognize sounds in noisier environments and in harsher conditions such as during rain or storm. It could possibly even compensate for lower quality hardware.

    \begin{figure}
        \centering
        \begin{subfigure}[b]{0.45\textwidth}
            \centering
            \includegraphics[width=\linewidth]{chapters/images/4. DA/fig spect before.pdf}
            \caption{Without noise}
        \end{subfigure}
        \begin{subfigure}[b]{0.45\textwidth}
            \centering
            \includegraphics[width=\linewidth]{chapters/images/4. DA/fig spect after.pdf}
            \caption{With noise}
        \end{subfigure}
        \caption{Spectrogram of a sample before and after adding noise}
        \label{fig:da:noise_spectrograms}
    \end{figure}

    However one should wonder if adding noise to the samples provides enough differentiation to avoid overfitting completely. When added to a waveform, noise seems to alter it dramatically, but if we look at the associated \glspl{spectrogram}, it only looks 'brighter' compared to the original sample--with details being washed out as a result. This is illustrated in \autoref{fig:da:noise_spectrograms}. This may compromise the quality of using this data augmentation type.

    Finally this method does not generate multi-label samples and thus will not help in training a model to recognize multiple classes at the same time.

    \clearpage\warn{clearpage check}

\section{Hybrid data augmentation}
\label{sec:da:hybrid}

    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig combined process.pdf}
        \caption{Hybrid data augmentation. The augmented datasets obtained through improved sample combining and noise sample augmentation are combined into a new augmented dataset.}
        \label{fig:da:hybrid_process}
    \end{figure}

    \begin{table}[t]
        \centering
        \begin{adjustbox}{width=\linewidth}
        \input{chapters/images/4. DA/tab samples hybrid}
        \end{adjustbox}
        \caption{The amount of samples per class combination after hybrid data augmentation and the total amount of samples per class.}
        \label{tab:da:hybrid_samples}
    \end{table}

    With different data augmentation techniques explored in the previous sections, we see that each technique has their own advantages and disadvantages. The best way to harness the advantages of both the improved cross-label sample combination in \autoref{sec:da:imp_combinations} and noise augmentation \autoref{sec:da:noise_augmentation} is to combine the augmented datasets to get a new, more varied augmented dataset. As illustrated in \autoref{fig:da:hybrid_process}, both the combined samples and noise augmented samples are generated separately, and then joined in a new dataset.

    This hybrid data augmentation technique should benefit from the advantages of both techniques mentioned. The improved sample combining ensures a model is prepared for multi-label input, and the noise sample augmentation creates balanced single-label samples. As shown in \autoref{tab:da:hybrid_samples}, the input dataset is almost perfectly balanced. It consists of single-label uncombined samples (original samples with pink noise added), single-label combined samples (samples gained by combining two samples with the same class) and multi-label combined samples (samples gained by combining two samples with different classes).

    % \clearpage\warn{clearpage check}

\section{Evaluation}
\label{sec:da:evaluation}

    % \begin{landscape}
    \begin{table}[t]
        \centering
        \begin{adjustbox}{width=\linewidth}
        \input{chapters/images/4. DA/tab experiments}
        \end{adjustbox}
        \caption{All trained models with the amount of training data and the time taken by data augmentation. Models \textit{None} and \textit{Naive} are baseline models. \textit{Serval} is trained on Serval cross-label sample combining as mentioned in \autoref{sec:da:serval}, \textit{Improved} is trained with Improved sample combining from \autoref{sec:da:imp_combinations}, \textit{Noise} was trained with the Noise sample augmentation described in \autoref{sec:da:noise_augmentation}, and finally \textit{Hybrid} was trained on Hybrid data augmentation as mentioned in \autoref{sec:da:hybrid}}
        \label{tab:da:experiments}
    \end{table}
    % \end{landscape}

    To test our hypotheses, all mentioned techniques were used to train a model. Additionally, as a baseline, a model was trained without any data augmentation, and a "naive rebalance" technique was used, wherein the samples are rebalanced without sample modification, thus serving identical samples multiple times. All techniques and their respective time taken on data augmentation and training can be found in \autoref{tab:da:experiments}.

    From the table it can be derived that all techniques using sample combination take less time per sample than techniques that do not employ sample combination. Furthermore, we can see that the "Serval cross-label sample combining" technique takes dramatically longer than all other methods. This can be explained partially by implementation details and the choice of library: as shown in \autoref{fig:da:library_times}, the Serval code for resampling files is dramatically slower compared to the new methods, taking around 60 times as long. Serval uses the Librosa\cite{mcfee_brian_2023_7746972} Python library for loading samples, the resampy library for resampling samples, and the soundfile library - based on libsndfile - for writing samples. The new methods on the other hand use the scipy\cite{2020SciPy-NMeth} library for both loading and writing files, and the audioop library for resampling. Finally, the new methods are optimized to not do any resampling when the sample rate is already the target rate, however in the making of \autoref{fig:da:library_times}, it was ensured that resampling is happening.

    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig library comparison.pdf}
        \caption{Time taken for read, resample, and write operations for Serval cross-label sample combining and for all other data augmentation methods}
        \label{fig:da:library_times}
    \end{figure}

    All models are trained using the Serval training code. This is a good baseline as other parts of Serval will be looked at in other chapters--if those parts are changed as well we cannot compare just the data augmentation results anymore. The model trained is a default Serval model with 10 \acrfull{lstm} cells.

    Unfortunately Serval does not do any validation, only testing. This makes it impossible to know during training whether a model is overfitting or underfitting or if it is training just right. This can be estimated after training by comparing the model predictions on both the validation and the training sets. Furthermore Serval does not report training accuracy either, making it even harder to rate how well a model in training is performing.

    Finally, some augmentation parameters still need to be decided. Serval's original parameters will not be changed, so in Serval cross-label sample combining, samples are resampled in 16000 Hz as required by VGGish, and dampened by 0dB, -6dB, and -12dB compared to the original samples. For each valid combination, up to 500 samples are created--sometimes the same random samples are picked and a new sample is not created, and some classes feature so few samples that 500 unique combinations cannot be created.\warn{Maybe add table of parameters}

    Improved sample combining uses the same resampling and dampening parameters, but creates only 250 samples per valid combination. This is because twice as many combinations are considered valid, so this results in roughly the same total number of samples.

    In Noise sample augmentation, samples are also resampled to 16000 Hz, but no dampening takes place. Instead, pink noise is added to the sample with a signal-to-noise ratio of a random value between 40 and 60. For each class 701 new samples are created--the maximum amount of training samples in a class.

    Finally in Hybrid data augmentation, the data sets generated by Improved sample combining and Noise sample augmentation are combined together in one dataset. The parameters used here are the same as mentioned above in the respective paragraphs.

    Now, the accuracy of the models needs to be measured. As mentioned before, Serval does not test during its training loop, so the test samples will be used for evaluation. Recall that the goal of the network is not only to predict classes in single-label samples, and we have only single-label samples in the evaluation set, as no data augmentation has been run on these samples. In order to still measure multi-label capabilities of the networks, a second validation set is generated in a similar fashion to the data augmentation in \autoref{sec:da:imp_combinations}, however, no time-shift is done and the volumes of the samples are unchanged.
    
    For each of the $36$ class combinations, a number of multi-label test sample are generated so that the total amount of multi-label samples matches the amount of single-label samples. For the new methods this means $19$ samples are generated for each combination, while for Serval  $146$ samples are generated.
    For Serval, more test samples are generated as Serval has more test samples due to the early dampening and the 50\%/50\%/ train/test split, however this also means that these test samples are modified and not necessarily unique: some of them may have been used for training.

    For each model and each validation set, we now predict the labels, and we take the average of the F$_1$ scores of all classes -- the harmonic mean of precision and recall as shown in \autoref{eq:da:f1}. This is a good metric for imbalanced datasets. Furthermore we measure the individual F$_1$ scores for each class.

    \begin{equation}
    \label{eq:da:f1}
        F_1 = \frac{2PR}{P+R}
    \end{equation}

    % Hypothese(s)?

    % \clearpage\warn{clearpage check}

\section{Results}
\label{sec:da:results}
    
    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig f1 results.pdf}
        \caption{F$_1$ scores for each model and each validation set}
        \label{fig:da:metrics}
    \end{figure}

    In \autoref{fig:da:metrics} we can see the results of the experiments mentioned in \autoref{sec:da:evaluation}. In terms of single-label F$_1$ scores, the model trained with hybrid data augmentation seems to work best, closely followed by the model trained without any data augmentation. One would expect the naive and noise data augmentations to show improved single-label F$_1$ scores as well however this does not seem to happen. Interestingly, the naive data augmentation does improve the multi-label F$_1$ score compared to doing no augmentation. This does make sense however if we consider that the single-label test dataset is as imbalanced as the original dataset, while the multi-label dataset is perfectly balanced. This causes the single-label F$_1$ score to decrease when the model is more balanced -- causing over represented classes to achieve a lower F$_1$ and under represented classes to achieve a higher F$_1$. In the (balanced) multi-label test dataset, the F$_1$ may increase depending on how much the under represented classes accuracy increase.

    Furthermore we see that Serval's data augmentation apparently does not do well with single-label samples, but scores a lot higher with multi-label samples. This can be attributed to the heavy focus of the data augmenatation on the combinations. This result causes the F$_1$ score for the combined dataset to be rather low as well. The same could be expected from the improved sample combining, but this technique scores much higher on single-label samples and comparable on multi-label samples. I cannot explain why this happens.

    Finally, when looking at all scores overall, the hybrid data augmentation appears to yield the best scores over all metrics. This is remarkable as the noise data augmentation by itself yields rather low results, but this combination shows no weakness whatsoever. This underscores the hypotheses stated in \autoref{sec:da:hybrid}.

    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/4. DA/fig f1 per class.pdf}
        \caption{F$_1$ scores for each model per sample with the combined single and multi-label test dataset}
        \label{fig:da:class_metrics}
    \end{figure}

    Next, \autoref{fig:da:class_metrics} shows the F$_1$ scores achieved by each model with the combined test dataset. It shows clearly that indeed the naive data augmentation is much more balanced compared to doing no data augmentation, however the gain of using noise augmentation is much more subtle. Furthermore Serval appears to somehow only increase the imbalance between classes. Finally the improved sample combining data augmentation scores better on most classes compared to doing no data augmentation and the Serval data augmentation, and the hybrid data mostly improves upon this with the lower-accuracy classes. Only amplified music appears to be much harder to recognize compared to other methods. This could partly be blamed by this being a rather messy class, with lots of samples featuring speech and car noises that are not labelled as such.