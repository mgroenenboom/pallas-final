\chapter{Conclusion}
\label{chp:conclusion}

    % Conclusion introduction
    This thesis describes the process of improving an existing sound recognition sensor, while exploring and applying the state of the art. As described in \autoref{sec:serval:problems}, Serval was based and built on rather outdated code. Furthermore the data augmentation technique used is not a common method and does not produce a well balanced dataset.
    
    % Short conclusion about each chapter
    %% Data augmentation
    This is why first the data augmentation technique was revised in \autoref{chp:da}. As shown in \autoref{sec:da:serval}, the method used shows some flaws. Multiple methods were tested of which a hybrid data augmentation technique using both an improved method based on Serval's, and a novel data augmentation technique using pink noise to differentiate samples are combined. As shown in \autoref{sec:da:results}, this hybrid method achieves the best F$_1$ scores for each test set: single-label samples, multi-label samples and the combined single and multi-label samples dataset.
    
    %% Embedding
    In \autoref{chp:embedding}, Serval's embedding, VVGish, was tested against multiple configurations of a more modern embedding, OpenL3. As shown in \autoref{sec:embedding_results}, not all configurations have the same impact. It is clear that using an OpenL3 embedding that was trained on a musical dataset outperforms OpenL3 embeddings trained on environmental data, which is surprising given our environmental dataset. The difference in performance between embeddings with different input representations is not as clear. However when considering the amount of extra time required for the mel-spectrogram embeddings, the linear spectrogram embeddings outperform the mel-embeddings. Thus the best OpenL3 configuration to use would be one trained on a musical dataset with linear spectrograms.
    
    %% Edge AI
    Finally \autoref{chp:edgeai} shows us that running OpenL3 is not as easy as running VGGish. On my PC, an experiment using OpenL3 takes about 31 times as long to run as the same experiment with VGGish. On the Raspberry Pi, this ratio is as high as 110 times as long. Unfortunately this makes it impossible to run the sensor with OpenL3 in real-time. To remedy this, this chapter explores different options to speed up OpenL3 execution. As shown in \autoref{sec:edgeai:results}, when running an embedding that was optimized for usage on an auxiliary device, the time required to run is reduced significantly to a point where running in real-time is possible. Furthermore power usage is decreased and the F$_1$ scores are slightly increased.
    
    \clearpage\warn{clearpage check}
    
    % Reiterate research questions
    In \autoref{sec:introduction:research_questions}, I posed several research questions. Here we reiterate them and try to answer them.
    
    \begin{itemize}
        \item Can we reduce the class imbalance and improve the accuracy of the Serval model without changing the dataset?

            In \autoref{chp:da} we learn that different data augmentation techniques may improve both the accuracy and the balance of the model. In particular the hybrid data augmentation shows higher accuracy for almost all classes and lower variance between class accuracies.

        \item Is changing the embedding enough to reduce imbalance or improve the accuracy of a model?

            We saw that switching VGGish to OpenL3 alone increases the accuracy of the downstream model considerably. Unfortunately this could not be a full comparison as Serval does not train on OpenL3 embeddings, but it appears that a Pallas model trained on an OpenL3 embedding performs much better than a Pallas model trained on a VGGish embedding.

        \item Can we get a model better than the Serval model while staying within computational and timing restraints?

            After changing the embedding used to OpenL3, it required a number of optimizations as detailed in \autoref{chp:edgeai}, but in the end Pallas runs with better accuracy and lower power usage compared to Serval. This did however require a hardware upgrade.

    \end{itemize}

    % Conclusion about entire paper
    In conclusion, I think this thesis answers the posed research questions well and shows a good insight in which methods were evaluated and why these were chosen. Some chapters could benefit from extended research, such as evaluating more data augmentation techniques and combinations in \autoref{chp:da} and more embeddings in \autoref{chp:embedding}. Because of uncertainties in the workload of the latter parts and lack of time, and the chapters being sufficiently clear, they were wrapped up in their current state.

\section{Process}

    % Short about process of work on thesis
    When I started working on this thesis, I started researching the Serval codebase. I quickly found that there were lots of inconsistencies and design choices that made the codebase hard to understand. Furthermore, my relative lack of a machine learning background did not help in understanding Serval and in developing the Pallas.

    After developing a basic framework for Pallas, containing alternative data augmentation methods, support for embedding with VGGish and OpenL3, and training the alternative Pallas network, I started writing the paper. Concurrently I started work on upgrading the device code. I quickly found that OpenL3 could not run fast enough on the Raspberry Pi, and that the code could run much faster when running different parts in parallel. Rewriting OpenEars to a parallel version would take as much time as starting from scratch, so this is when I started work on the new multi-threaded Pallas device code.

    While working on the paper I found that I did not explore enough data augmentation methods and OpenL3 configurations. After adding the new data augmentation methods mentioned in \autoref{chp:da} and OpenL3 configurations mentioned in \autoref{chp:embedding}, I found that my original methods were worse than these new methods. After running all steps again I got the final results as shown in the paper.

    Remaining now is only the paper. During the last part of the thesis I only worked on the paper and multiple presentation opportunities. After some iterations I present to you the final version of this paper.

    % What would I have done differently in hindsight?
    In hindsight the biggest problems were my lack of a solid machine learning background and the somewhat rash approach at the start of the thesis. This forced me multiple times to redo or rerun parts of previously done work. If I had started with this breadth-first approach starting with data augmentation and working from that on, instead of this depth-first approach of trying to get everything working and then fine-tuning every part, a lot of time could have been saved.

\section{Future work}

    % Future work:
    As stated before, some parts of the thesis are not perfect yet at the end. This section details which steps can be taken to improve upon this work.

    %% Improve used dataset
    First of all, a large part of this thesis is focused on working around class imbalance. Furthermore this class imbalance in the dataset makes it hard to use the usual 80\%/10\%/10\% train/validation/test split, as 10\% of the 23 moped alarms samples would come down to having only 2 test and 2 validation samples for this class. This does not give us a representative accuracy. These problems are solved when the class imbalance is not as apparent in the used dataset. Finally there are certain classes that may be added to avoid false positives or for further interest, such as fireworks (to avoid false positives with gunshots), wind or rain noise, airplanes and vehicle sirens.

    %% Explore more data augmentation techniques and combinations
    \Autoref{chp:da} about data augmentation evaluates multiple data augmentation options, and concludes that a hybrid data augmentation technique using multiple data augmentations shows the best potential. However, many promising options could be evaluated, as well as more combinations of multiple techniques. Finally a pipeline approach was not tested, where, for example, noise is added to each combined sample. These approaches could be considered for future exploration.

    %% Explore more embeddings
    In \autoref{chp:embedding}, VGGish was tested against some configurations of OpenL3. No comparison was made with other embeddings due to time constraints. Testing an embedding is not that simple. These embeddings were chosen because they are readily available and easy to use, but for other embeddings this may not be the case. Nevertheless other embeddings may show promising results, and these could be taken in consideration.

    %% Improve Pallas model
    As mentioned in \autoref{sec:emb:pallas}, the Pallas model is a simplified version of the Serval model. This approach was chosen partly because of my lack of a machine learning background and due to time considerations. However this model may be improved by using additional layers, for example the \acrshort{moe} layer that Serval uses. Furthermore layers can be added that assist in training, such as noise or dropout layers. Finally many loss and optimizer functions were tried. Better results may be achieved after applying more fitting loss, optimizer and activation functions.
