\chapter{Project inspiration: Serval}
\label{chp:serval}

    \note[hide]{
        Serval links:
        https://sensingclues.org/
        https://sensingclues.org/serval/ (broken)
        https://sensingclues.org/news/category/Serval+Sensor
        https://web.archive.org/web/20220418023349/https://sensingclues.org/serval/
        https://github.com/SensingClues/serval
        
        OpenEars links:
        https://github.com/SensingClues/OpenEars
        https://sensingclues.org/
        
        Sensemakers links:
        https://www.sensemakersams.org/
        https://www.ams-institute.org/
    
        https://github.com/google/youtube-8m
        https://github.com/devicehive/devicehive-audio-analysis
        https://www.raspberrypi.com/products/raspberry-pi-3-model-b/
        https://www.minidsp.com/products/acoustic-measurement/umik-1
    }

    This thesis is based on Serval\footnote{\url{https://sensingclues.org/news/category/Serval+Sensor}}(Sound Event Recognition-based Vigilance for Alerting and Localization), a product currently being developed by SensingClues\footnote{\url{https://sensingclues.org/}}: a Dutch foundation that develops smart solutions for nature conservation and wildlife protection, in collaboration with Sensemakers\footnote{\url{https://www.sensemakersams.org/}}: an Amster\-dam-based community that focuses on IoT and smart city solutions. The purpose of Serval is to help with wildlife monitoring in remote areas. This chapter explains what Serval is and what can be improved. In addition, it presents potential concrete improvements that we propose.

    % \clearpage\warn{clearpage check}

\section{Serval}
\label{sec:serval}

    \begin{figure}[p]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/3. Serval/fig openears.jpg}
        \caption{An opened OpenEars device}
        \label{fig:serval_openears}
    \end{figure}

    \begin{figure}[p]
        \centering
        \includegraphics{chapters/images/3. Serval/fig serval diagram.pdf}
        \caption{The layers that the Serval model and the Youtube8M model consist of}
        \label{fig:serval_model}
    \end{figure}

    Serval\footnote{\url{https://github.com/SensingClues/serval}} is an algorithm for training a sound recognition model. It is being developed in conjunction with OpenEars\footnote{\url{https://github.com/SensingClues/OpenEars}}, a software + hardware platform to deploy smart microphones and a server with a dashboard that displays the measurements and predictions of these sensors. One of these devices is shown in \autoref{fig:serval_openears}. These sensors are intended to be used in wildlife reserves to track wildlife behavior - particularly elephants' - and human presence in the form of chainsaws, engine sounds, and gunshots. Sounds travel much further than visuals, making it possible to map large areas with relatively few and relatively inexpensive sensors. Mapping wildlife behavior is invaluable in scenarios where such wildlife needs to to be protected. Measuring human presence enables rangers to identify early on where poachers and illegal loggers are active, allowing them to more effectively combat them.

    Serval is based on the Youtube-8M Tensorflow Starter Code\footnote{\url{https://github.com/google/youtube-8m}} repository, a demo for using the Youtube8M\cite{abuelhaija2016youtube8m} dataset. This demo and Serval are powered by Tensorflow 1\cite{tensorflow2015-whitepaper}, a machine learning framework for Python. Serval also uses data augmentation to reduce the class imbalance in the dataset and increase the overall amount of samples. Finally the embedding VGGish\cite{vggish-paper} is used to extract features from audio segments, facilitating the training of the Serval model. The precise layout and nature of VGGish is detailed in \autoref{chp:embedding}.

    The Serval model consists of two sub-models as shown in \autoref{fig:serval_model}: a frame-level model that runs on the variable-length list of frames of which the video (or audio) consists and a video-level model that runs on the output of the frame-level model. The frame-level model is a \acrfull{lstm} model consisting of 1 layer of 10 \acrshort{lstm} cells and the video-level model consists of a \acrfull{moe} model as proposed by Jordan and Jacobs \cite{MOE_paper}. This \acrshort{moe} model is implemented as two fully connected layers that are multiplied by each other. The model is intended to recognize which sound classes can be heard in the sample. To do this it calculates the probabilities that a given class is featured in the sample, after which this is compared with a predetermined threshold to decide whether this class will be considered to be present or not.

    OpenEars is powered by a Raspberry Pi 4 Model B Rev 1.1 \footnote{\url{https://www.raspberrypi.com/products/raspberry-pi-3-model-b/}} and is being developed by SensingClues and Sensemakers. It uses a UMIK-1 omnidirectional USB microphone\footnote{\url{https://www.minidsp.com/products/acoustic-measurement/umik-1}}. The software is based on devicehive\footnote{\url{https://github.com/devicehive/devicehive-audio-analysis}}, an open source IoT data platform, and communication is done using MQTT, a lightweight messaging protocol.

    Additionally, Serval is coupled with an urban sound dataset collected by Waag Society and Sensemakers in the city of Amsterdam. The main goal of Sensemakers is to gain insight into the intensity and sources of noise pollution and signs of criminal activity while further developing the wildlife sensor in an easier environment.

    % \clearpage\warn{clearpage check}

\section{Problems with Serval}
\label{sec:serval:problems}

    Serval has some issues that need to be addressed. It is based on the Youtube-8M demo code, which is rather outdated (the most recent updates date back to 2019) and is powered by Tensorflow 1, which is no longer supported.

    A mayor issue with Serval itself is caused by its structure. Serval is composed of 12 Jupyter (A web based interactive coding environment)\cite{Kluyver2016jupyter} notebooks. These notebooks are great for interactive and collaborative work, but a lot harder to use from the command line or in an automated environment. Furthermore, a copy of the configuration is stored in duplicate in each notebook, making it difficult and prone to errors to change a single configuration or path, as these need to be changed in every file.

    Finally, the accuracy of the model is inadequate, making it impossible to deploy the system in the real world, making this the most pressing problem to solve.

    \clearpage\warn{clearpage check}

\section{Proposed solutions: Pallas}

    \begin{figure}[t]
        \centering
        \includegraphics[width=0.84\linewidth]{chapters/images/3. Serval/fig serval v pallas.drawio.pdf}
        \caption{Serval's components on the left compared to Pallas' components on the right}
        \label{fig:serval:pallas_comparison}
    \end{figure}
    
    In order to address both the issue of being outdated and the issues with its structure, the entire codebase must be rewritten. This new codebase goes by the name of Pallas. Pallas still uses Jupyter notebooks, but functions and constants are moved to separate Python files to enhance usability. Pallas is powered by Tensorflow 2 and a single configuration is used by all other functions and scripts.

    To improve the accuracy of the model, several aspects must be revised.
    First, the data augmentation part of Serval is improved. Since the dataset is imbalanced and does not accurately reflect the final objective of the model, the dataset is augmented before being used to train the model. As this is a critical step in Serval, the data augmentation method used by Serval is evaluated and improved.

    Also, the embedding is replaced with $L^3$-Net\cite{l3-paper}, or specifically OpenL3\cite{openl3-more-paper}, an open-source Python library that makes $L^3$-Net much easier to use. This helps to improve the model's accuracy. The downside of this change is that OpenL3 and the embeddings it generates are a lot larger than VGGish and its embeddings.

    Finally, the OpenEars code is powered by TensorFlow 1 as well. As Pallas is powered by TensorFlow 2, certain work is necessary to run this new code on the device. Also, the device software and hardware must be optimized to ensure the much heavier OpenL3 model is able to run in real-time.

    All components of Serval and their updated components in Pallas can be found in \autoref{fig:serval:pallas_comparison}.
