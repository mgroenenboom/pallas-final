\chapter{Neural Networks in Embedded Systems}
\label{chp:edgeai}

    When using a neural network in an embedded system, new problems arise. Neural networks may require large amounts of memory and processing capacity. Most embedded systems do not have these specifications as they are designed to be as inexpensive and energy efficient as possible.

    One solution to this problem is running as much as possible in the cloud, however this requires sending large amounts of identifiable information to a remote server. This data could be partially anonymized by running the pre-processing on the edge, but with audio networks one may find that the pre-processing takes more time than running the neural network itself. Some solutions have been devised to train and run networks with obfuscated data in the cloud; however, these typically diminish accuracy of the models. More recent solutions with noise injection however produce near-perfect models\cite{tudelft-privacy-cloud-dnn} compared to regular models.

    Another solution is to use TensorFlow Lite\footnote{\url{https://www.tensorflow.org/lite}} to run the models on the device instead of full TensorFlow. This library is adapted for use on mobile and edge devices and may prove more feasible to run thanks to its lower memory and computational footprint.

    A final solution is to accelerate the embedded system with a \acrfull{tpu}\todo{\cite{}}. This would mean adding a peripheral computation device optimized for working with tensors. Tensors are the building blocks of neural networks and the namesake of TensorFlow. These TPU's are able to compute tensor calculations magnitudes faster and more efficient compared to CPU's and GPU's. The downside of using a TPU is that it requires adding this potentially costly device to each deployed device.

    % \clearpage\warn{clearpage check}

\section{OpenEars}

    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/6. Device/fig openears with tpu 2.jpg}
        \caption{An opened OpenEars device with the Coral TPU added}
        \label{fig:edge_oe+tpu}
    \end{figure}

    The hardware used by Serval is called OpenEars\footnote{\url{https://github.com/sensemakersamsterdam/OpenEars}}. OpenEars is an open source project based on devicehive\todo{\cite{}}\footnote{\url{https://github.com/devicehive/devicehive-audio-analysis}} and is being developed by SensingClues and Sensemakers AMS. Its hardware consists of a Raspberry PI 4b and USB microphone -- a UMIK-1\footnote{\url{https://www.minidsp.com/images/documents/Product\%20Brief\%20-\%20Umik.pdf}}. Furthermore, MQTT is used to communicate with a Grafana Dashboard server. A Coral TPU\footnote{\url{https://coral.ai/static/files/Coral-USB-Accelerator-datasheet.pdf}}\todo{\cite{}} can be connected to improve its tensor computational abilities.
    An OpenEars device with such a Coral TPU added is shown in \autoref{fig:edge_oe+tpu}.

    % \clearpage\warn{clearpage check}

\section{Pallas device code}

    \begin{figure}[p]
        \centering
        \includegraphics[width=\linewidth,height=\textheight,keepaspectratio]{chapters/images/6. Device/fig pallas pipeline.pdf}
        \caption{The pipeline pallas uses with the threads (in the boxes) and the data exchanged between threads (in the circles). Note that most threads are interchangable with alternative threads, like the loading thread and the microphone thread.}
        \label{fig:pallas_pipeline}
    \end{figure}

    Pallas uses the same hardware as Serval, however, all device code is rewritten to improve coding standards and to upgrade its Python and Tensorflow versions. As shown in \autoref{sec:edgeai:results}, the pre-processing step and the embedding step take roughly the same amount of time. Especially if the embedding is done on a TPU, these two steps can very well be run in parallel. This is why Pallas uses a multi-threaded pipeline to improve prediction speed. A flow chart of this pipeline can be found in \autoref{fig:pallas_pipeline}.

    From this flowchart is it clear that the Pallas codebase is designed with backwards compatibility and modularity in mind. For the input a file system loading thread can be used, while the microphone thread uses live recorded sound data as its input. Both threads allow setting the target sample rate, and produce a waveform and its sample rate. The loading thread optionally allows retrieving the true label if a true label is given.

    Preprocessing for Pallas can be done with the Librosa library and with Kapre. As described in \autoref{sec:embedding_openl3}, OpenL3 can use both Librosa and Kapre as its front-end. VGGish requires its own specific preprocessing implemented by the VGGish preprocess thread.

    As for the embedding, Pallas allows creating VGGish and OpenL3 embeddings. The VGGish embedding can then be used with both Serval and Pallas, while the OpenL3 embedding can only be used with Pallas. These prediction threads then produce a logit (also called log-odds) for each trained class: the logarithm of the odds that a sample contains a given class. These logits are finally used in the output threads. The exact behavior depends on the thread: The saving thread saves the logits and optionally the true labels and system performance statistics to the file system. The MQTT thread sends the logits and some system performance statistics to the configured MQTT host. Finally the display thread calculates the predicted classes using an optimal threshold attained through testing with labelled samples. These predicted classes and some system performance statistics are displayed in a console and/or saved to a log file for offline usage.

    % \clearpage\warn{clearpage check}

\section{TensorFlow Lite}
\label{sec:edgeai_tflite}

    As mentioned, one option for improving operation speed and power usage is to use TensorFlow Lite models. To do this, the models first need to be converted using the built-in TensorFlow Lite converter. Additionally, this converter allows for a number of optimizations such as quantization, pruning, and clustering. Quantization reduces the precision and size of the model's variables, allowing for a smaller network size and faster computation, while compromising accuracy. Pruning and clustering only reduce model download size with no benefit to computation speed, so these optimizations are left aside.

    TensorFlow Lite's quantization has a few configuration options, of which some can be used concurrently. However, due to TensorFlow Lite's complexity and many experimental options, a TensorFlow Lite guide\footnote{\url{https://www.tensorflow.org/lite/performance/post_training_quantization}} is used to determine some types of quantization that can be tried. The most obvious optimization is to change the datatype to a smaller and simpler datatype. By default TensorFlow models use 32-bit floating point number to store weights and biases. Using the TensorFlow Lite Converter, the variables can be converted to 16-bit floating point, 16-bit integer, and 8-bit integer. Finally an experimental option is present that allows storing activations as 16-bit integers, weights as 8-bit integers and biases as 64-bit integers. This method should improves accuracy compared to other optimizations and only slightly increase the size of the model.

    Converting 32-bit floating point values to 16-bit floating point is a trivial operation: nominal values found in our neural networks are well within the range of both 32-bit and 16-bit floats. However this conversion will negatively affect the precision of the model. Converting to integer numbers is not as trivial, as converting floating point numbers to integers in general comes with a penalty in accuracy. To mitigate this by using the full available range of integers, the value is normalized to the full range of the target datatype. In order to do this, TensorFlow determines the range of the variables dynamically using sample inputs. Integer operations are much faster on most hardware, so this conversion should yield a model that is both smaller and faster.

    Running a TensorFlow Lite model is straightforward: the model is loaded into a TensorFlow Lite Interpreter. This object manages the tensors and the operation of the model. If the input of the model is quantized, one has to quantize the input before calling the interpreter.

    % \clearpage\warn{clearpage check}

\section{Edge TPU}

    Another option involves using a \acrfull{tpu}. These devices considerably speed up the operation of a neural network, but before a model can be deployed on one, it needs to be compiled first. Google's Coral TPU offers a compiler for TensorFlow Lite models. Not all TensorFlow Lite models can be compiled for the Coral TPU, as it only supports 8-bit integer operations. In \autoref{sec:edgeai_tflite} it was described how an 8-bit integer quantized model can be created. This model can then be compiled to yield a Coral-compatible TensorFlow Lite model.

    Running such a Coral-compiled model is slightly more cumbersome than running a regular TensorFlow Lite model. First there are some additional software dependencies that must be installed. The compiled TensorFlow Lite model can then be loaded into the interpreter. To ensure this interpreter attempts to run on the Coral TPU, the Coral TPU library is passed to the interpreter as a so-called "delegate". These delegates allow the interpreter to use hardware acceleration through internal or external libraries.

    % \clearpage\warn{clearpage check}

\section{Evaluation}
\label{sec:edgeai:evaluation}

    To find out what optimizations are necessary to run Pallas on the embedded device, we need to measure the performance of all optimization options. As some optimizations may incur a penalty in model accuracy, both the model accuracy and the time and resource performance must be measured.

    As shown in \autoref{sec:embedding_results}, running the Pallas model takes a negligible amount of time compared to the preprocessing and embedding steps. As the preprocessing happens in a secondary library, it cannot be accelerated using TensorFlow Lite, so only the OpenL3 model will be converted to TensorFlow Lite.
    
    Running OpenL3 on the Raspberry Pi takes much longer compared to running it on a PC, at almost two minutes per 10-second sample, compared to half a second on a PC. This renders running the full test dataset infeasible, considering it consists of 1363 such samples, bringing the total time taken for running the entire test dataset up to around two days. Thus, running multiple experiments would take too long. To avoid this, a smaller dataset was created consisting of only 54 samples. If an experiment proves to run fast enough, it can be repeated with the full dataset to yield a better comparison with earlier experiments.

    All experiments can be seen in \autoref{tab:edgeai_experiments}. These experiments will be ran and timed. The same timing statistics and F$_1$-scores as in \autoref{sec:embedding_evaluation} are produced. All experiments that run fast enough to run in real-time, i.e. that finish in at most 540 seconds will be run again with the complete test dataset to compare the accuracy of all models. These models will then be compared against the accuracy of Serval and the accuracy of Pallas when using an unoptimized OpenL3 model.

    \begin{figure}[t]
        \centering
        \includegraphics[angle=-90, width=0.50\linewidth]{chapters/images/6. Device/fig energy testing.jpg}
        \caption{The setup for measuring power consumption. A BASETech EM-3000 is placed between the power outlet and the Raspberry PI power adapter, measuring power usage of the device.}
        \label{fig:edgeai_power_consumption}
    \end{figure}

    Finally for both Serval and Pallas the power consumption is measured. This is done using a BASETech EM-3000\footnote{Datasheet downloaded at 7 February 2024 from \url{https://asset.conrad.com/media10/add/160267/c1/-/gl/001611632ML01/gebruiksaanwijzing-1611632-basetech-em-3000-energiekostenmeter-kostenprognose.pdf}}. The full setup used can be seen in \autoref{fig:edgeai_power_consumption}. The BASETech EM-3000 has a power consumption of up to 1W and a precision of 2\%. This means it is not the highest precision device available for this scenario. To remedy this, power consumption is measured over at least 24 hours. Because the own power consumption of the BASETech EM-3000 is not clear, it is included in the measured values.

    \begin{table}[t]
        \centering
        \begin{tabular}{@{}r|llr@{}}
            \multicolumn{1}{l|}{Model ID} & Optimization     & Model size & Optimization duration \\ \midrule
            0 & None             & 17.9 MB    & N/A                 \\
            1 & TFLite           & 17.8 MB    & 4s                  \\
            2 & Dynamic range    & 4.5 MB     & 4s                  \\
            3 & Full integer     & 4.5 MB     & 3317s               \\
            4 & 16-bit float     & 8.9 MB     & 3s                  \\
            5 & 16x8 int         & 4.5 MB     & 6637s               \\
            6 & Full integer TPU & 4.5 MB     & 3310s              
        \end{tabular}
        \caption{Experiments and their optimization types, compiled model size and time taken by the optimization process itself}
        \label{tab:edgeai_experiments}
    \end{table}

    % \clearpage\warn{clearpage check}

    We can formulate the following hypotheses:
    \begin{enumerate}
        \item Integer models are faster than Floating point models.

            We expect that the TensorFlow Lite models using integer variables are faster than the TensorFlow Lite models using floating point variables. Furthermore, the TensorFlow Lite models using floating point variables are expected to be faster than the original model by a small margin.

        \item The TPU-compiled model is faster than all other OpenL3 models.

            Models running on the TPU may both be faster and slower compared to their original models running on the CPU. OpenL3 is not the largest model so we expect the TPU-compiled TensorFlow Lite model to be faster than all other OpenL3 optimizations and the original.

        \item The accuracy of TensorFlow Lite optimized models (models 2 to 6) is less than the original model by a small margin.

            The trade-off for faster models is less accurate models, so it is to be expected that the faster a model is, the less accurate it is.

        \item TensorFlow Lite optimized models require less computational resources compared to the original model.

            TensorFlow Lite is optimized to use fewer resources compared to the original model. Especially the TPU-compiled model is expected to use less resources as the model will run on the TPU, freeing up resources on the CPU and in the RAM.

		\item Running with a TPU requires less power than running without TPU.

            As the TPU-compiled model requires less resources on the CPU, and the TPU requires a fraction of the power a CPU or a GPU requires for running neural networks, it is expected that running a large model on the TPU is much more energy efficient compared to running the same model - or even a smaller model - on the CPU.

    \end{enumerate}

    \clearpage\warn{clearpage check}

\section{Results}
\label{sec:edgeai:results}

    % // Results experiment 1

    \begin{figure}[t]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/6. Device/fig experiment 1 usage.pdf}
        \caption{CPU and RAM usage for each model. The CPU usage measured is the average combined core usage, so 100\% means all cores have 100\% usage. The RAM usage is the maximum RAM usage seen during execution for the relevant python processes.  The total amount of RAM available on the OpenEars device is 4 GB.}
        \label{fig:edgeai:experiment1_usage}
    \end{figure}

    From \autoref{fig:edgeai:experiment1_usage} it appears that while most models still show high CPU-usage, RAM usage is reduced in all optimized models. This can partially be explained by the original OpenL3 model requiring the full TensorFlow library, while the optimized models only require the TensorFlow Lite runtime. Also note that the 5th model and the 6th model do not fully use the CPU. In the case of the 6th model this makes sense, as this model mostly runs on the TPU instead of the CPU. However, for the 5th model, this could only be attributed to how TensorFlow Lite runs this model. As the 5th method uses an experimental optimization it may be the case that its execution was not optimized yet, or it was unable to run multi-threaded (its slightly-more-than 25\% CPU usage corresponds to a single core of the four available cores being used).

    \begin{figure}[p]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/6. Device/fig experiment 1 F1.pdf}
        \caption{Final metrics for each model on each test set in experiment 1. The model ID's correspond to the same model ID's found in \autoref{tab:edgeai_experiments}. Do note that these results were made with the slim dataset, so they cannot be compared to other F$_1$-score results.}
        \label{fig:edgeai_experiment1_F1}
    \end{figure}

    \begin{figure}[p]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/6. Device/fig experiment 1 timing.pdf}
        \caption{Time taken by each model in each step in the prediction timeline during experiment 1. The model ID's refer to the same model ID's found in \autoref{tab:edgeai_experiments}}
        \label{fig:edgeai_experiment1_timing}
    \end{figure}

    In \autoref{fig:edgeai_experiment1_F1} it is clear that single-label performance did not suffer from the optimizations, however the multi-label F$_1$-scores appear to be drastically reduced for the integer-based models (3, 5 and 6) while the float-based models (0, 1, 2, and 4) do not show any difference. This shows that integer quantization indeed impacts the model accuracy, while using reduced-accuracy floating-point variables barely impacts accuracy at all. Interestingly enough the performance hit only seems to be apparent for single-label samples. Possibly these single-label samples are easier to recognize and thus do not suffer from the integer quantization.

    Regarding the timings found in \autoref{fig:edgeai_experiment1_timing}, it appears that the integer-quantized models (3, 4, and 6) are much faster than the floating point models (0, 1, and 2), except for the 16x8 integer model (model 5). Furthermore, the TPU-compiled model (model 6) is much faster than its alternatives and is able to run in real time. This means this is the only model that will actually be tested in the next experiment with the large dataset.

    % \clearpage\warn{clearpage check}

    % // Results experiment 2

    \begin{figure}[p]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/6. Device/fig experiment 2 F1.pdf}
        % Updated!
        \caption{Final metrics for each model on each test set in the second experiment. Note that these results were obtained from testing the full test dataset.}
        \label{fig:edgeai_experiment2_F1}
    \end{figure}

    Running the Serval model with VGGish, Pallas with the original OpenL3 model and Pallas with the TPU-compiled OpenL3 model on the full dataset mentioned in \autoref{sec:embedding_evaluation} produces the results shown in \autoref{fig:edgeai_experiment2_F1}. As shown in this graph, the TPU-compiled model actually outperforms both the Pallas model with the original OpenL3 and the Serval model with VGGish.

    \begin{figure}[p]
        \centering
        \includegraphics[width=\linewidth]{chapters/images/6. Device/fig power usage.pdf}
        \caption{Power usage of the Raspberry Pi in different usage scenarios}
        \label{fig:edgeai:powerusage}
    \end{figure}

    Finally as shown in \autoref{fig:edgeai:powerusage}, the power usage of the different experiments does not differ much. Serval uses only slightly more power in parallel than it does in serial usage, however in Pallas we see a larger difference. This is probably due to the smaller embedding used by Serval and thus less overhead due to queuing and serialization necessary for parallel execution. Also clear is that Pallas in serial usage uses less power than Serval in serial. This can be attributed to Pallas running its embedding on the optimized TPU device instead of on the CPU.

    % // Hypotheses

    % \clearpage\warn{clearpage check}
    
    Looking back at the hypotheses formulated in \autoref{sec:edgeai:evaluation} we can conclude the following about them:
    \begin{enumerate}
        \item Integer models are faster than Floating point models.

            From \autoref{fig:edgeai_experiment1_F1} we can conclude that indeed most integer models are faster than floating point models. Only the 16x8 integer model fails to run in reasonable time. Considering that this is an experimental optimization, we can conclude that it was probably not designed yet to run efficiently.

        \item The TPU-compiled OpenL3 model is faster than all other OpenL3 variants.

            This model indeed outperforms all other variants based on OpenL3. Only the Serval model, which uses VGGish, is still a lot faster.

        \item The accuracy of TensorFlow Lite optimized models (models 2 to 6) is less than the original model by a small margin.

            In \autoref{fig:edgeai_experiment1_F1} it is shown that the models using TensorFlow Lite's integer quantization indeed score lower F$_1$-scores compared to their floating point quantization counterparts, however in \autoref{fig:edgeai_experiment2_F1} this dip in accuracy cannot be seen. On the contrary, the TPU-compiled OpenL3 model appears to score a slightly higher F$_1$ score compared to the original OpenL3 model.
            
            This difference could be explained by the size of the smaller dataset: in such a small set, artifacts are more probable due to the random selection of samples from the full set, the difference shows that the selection may happen to contain mostly more difficult samples.

        \item TensorFlow Lite optimized models require less computational resources compared to the original model.

            As shown in \autoref{fig:edgeai:experiment1_usage}, the integer quantized models are much faster and use less ram compared to the float-quantized models. However, the 5th model is an exception to this: It appears that it was unable to fully use the computational resources the device has to offer, causing it to be much slower compared to all other models.
            
            The float-quantized models do not show much better performance compared to the original model.
            
            Regarding RAM usage, all optimized models perform much better than the original model, sometimes requiring less than half the RAM usage the original model does.
            
		\item Running with a TPU requires less power than running without TPU.

            It is clear from \autoref{fig:edgeai:powerusage} that Pallas when running in serial and avoiding parallelization overhead is much more efficient compared to Serval doing the same thing.

    \end{enumerate}
