\chapter{Required background and related work}
\label{chp:related}

\section{Acoustic Event Detection}

    % Intro
    Serval can be seen as a potential answer to the \acrfull{aed}, or specifically the \acrfull{esc} problem: recognizing and categorizing sound events. These solutions are designed to constantly monitor sounds from the environment and detect and classify certain sound types. In the case of Serval, these are either urban or natural sounds, depending on the dataset used to train the model.

    % SONYC
    Comparable projects have been implemented around the world to measure noise pollution. Recently, researchers from New York University\cite{sonyc} worked on a similar sensor network deployed in New York City, called SONYC -- Sounds of New York City. Both SONYC and Serval focus on measuring and analyzing environmental sounds. The main difference between these projects is that SONYC defines a complete framework that not only measures and analyses environmental sounds, but also visualize it and even formulate ways to actuate on it. Serval and Pallas on the other hand focus on (autonomous) hardware that analyses data on the edge, and reports to a client.

    % IDEA
    Another earlier project is the IDEA (Intelligent Distributed Environmental Assessment) project \cite{idea} from Flanders in Belgium, IDEA can be seen as a standard sensor network. The main difference in the sensors is that the sensors do not do any analysis but transmit the data to a server. Connected to this server are autonomous agents doing various kinds of analysis. Serval and Pallas of course do this analysis on the edge, to minimize the amount of data sent and reduce the privacy impact. Finally this approach requires a massive data collection and storage server, serving not only multiple sensors but also multiple analysis agents. Serval and Pallas on the other hand do not require such a server. For these only a small server would suffice.

    % MESSAGE
    A final project that was examined is MESSAGE\cite{BELL2013169} (Mobile Environmental Sensing System Across Grid Environments) from Newcastle University, deployed in the UK and Palermo, Italy. Again, this project has a rather different focus compared to Serval and Pallas. This project focuses on two parts. First focus point is on the hardware. An inexpensive autonomous \textit{mote} was developed, in order to make it cheaper and more attractive to deploy. Serval is still in its development phase, using a prototyping platform for its development. Furthermore, these \textit{motes} do not support sound recognition, only sound pressure levels are recorded. The second focus is on clustering the sound pressure levels recorded depending on their environments: The type of road they are placed at, as well as the presence of objects such as schools, traffic lights and bus stops, and the effect of wind strength and wind direction compared to the street direction. The aim of Serval is the opposite, to sense at larger scales, to detect wildlife and human presence, and not local effects.

    % Outro
    Of course, there are many more projects that focus on this problem. A handful of promising and differing solutions was chosen for comparison, while there may be more similar solutions.

    %\clearpage\warn{clearpage check}

\section{Machine learning}

    % Intro
    Machine learning is a broad field with even broader application. It uses statistics and generalization to train models that are able to do a plethora of tasks, including classification, segmentation, and recently even content generation. Serval uses it both in its embedding, VGGish, which is a \acrfull{dcnn}, and the downstream model, a \acrlong{lstm}, which is a type of \acrfull{rnn}.

    % ESC CNN (ConvNet)
    One of the earliest researches into \acrshort{esc} with \acrshort{dcnn} was done by Piczak\cite{ESC_CNN} in 2015. He considered that while \acrshort{dcnn} has shown promising results in multiple fields already, including sound analysis such as speech recognition and music analysis, no attempt was made yet to apply \acrshort{dcnn} to \acrshort{esc}. This research shows promising results, especially considering its datedness.

    % ESC attention crnn
    Another relatively new type of \acrshort{rnn} is the attention-based model. This model is able to focus on different weights, depending on the context. Zhang et al.\cite{ESC_ACRNN} used this method to accurately do \acrshort{esc}, albeit with a limited dataset. This method shows promise however, especially when considering the size of the model. Attention might be an interesting option to explore apart from using an \acrshort{lstm}.

    % T3
    Jatturas et al.\cite{ESC_feature_vs_dcnn} compare older feature-based methods with newer \acrshort{dcnn} methods, comparing support vector machine (SVM) and multilayer perceptron (MLP) methods with a method consisting of a \acrshort{dcnn} model for its feature extraction with fully connected layers as the classifier. The latter is similar to what Serval and Pallas are doing, using an external \acrshort{dcnn} model as its embedding to do the feature extraction. The main difference with this paper is that Serval and Pallas use an \acrshort{lstm} instead of simply fully connected layers, although fully connected layers are still used for classification after the \acrshort{lstm}.

    \clearpage\warn{clearpage check}

\section{Data augmentation}
\label{sec:SoA_DA}

    In order to train any machine learning model, one must have training data. This data can be either labelled or unlabelled, depending on the type of machine learning. Depending on the complexity of the problem, it may be necessary to increase the amount of data or diversify the existing data. Data augmentation is a technique to modify existing training data or generate new training data.
    
    This method can be applied to both image and audio data. The audio data is usually converted to a visual representation, a spectrogram, and then data augmentation can be applied to the spectrogram. This allows one to use image data augmentation methods on audio data. Many data augmentation techniques exist for images, with differing effectiveness and efficiency, as shown by Shorten and Khoshgoftaar\cite{survey_data_augmentation}. These techniques include basic transformations such as flipping, rotation, panning, cropping, and noise injection. Other options include altering the brightness of the image, or any other color space transformations when using colored images, if possible in the problem context. Finally more complex techniques may randomly mix images, cut parts out, or even use specialized neural networks for generating completely new data.
    
    When dealing with audio, there are more options besides image data augmentation on the spectrogram. One option is to combine multiple samples together. This can be done in multiple ways: Takahashi et al. \cite{takahashi_interspeech} propose that combining samples of the same class yields new samples of that class: A sample containing two (different) cars should be tagged as a sample with cars. In multi-label classification, samples from different classes can be combined to yield multi-label samples: A sample containing both a car and a gunshot should be tagged as having both car sounds and a gunshot in it, in a multi-label classification task. This is a novel type of data augmentation that Serval uses. Other options include injecting noise into the samples, performing a time shift on the samples, or changing the speed, pitch or volume of samples, as discussed by Salamon and Bello \cite{cnn_data_augmentation_esc}.

    Serval uses data augmentation to achieve two goals: first of all to rebalance the dataset, and secondly to get multi-label samples that are not present in the original dataset. This technique and other options will be evaluated to improve the final accuracy.

    % \clearpage\warn{clearpage check}

\section{Embedding}
\label{sec:SoA_Embedding}

    Training a deep neural network to complete a complex task such as \acrfull{aed} requires a large dataset and a lot of computing power. An embedding is a relatively new solution that can be used when these requirements are not met. It consists of a model that extracts features from an input that can be used to complete the \acrshort{aed} task. An example of this is a model that extracts facial features for face recognition. When using such an embedding, one only needs to train a model to complete the \acrshort{aed} task using the features as input.

    There are numerous examples of embeddings that have recently been developed. An early example is SoundNet, developed by Aytar et al.\cite{soundnet-paper} in 2016 at Massachusetts Institute of Technology. It uses a student-teacher model to train a \acrlong{dcnn} to extract features from samples, which are validated by a one-layer SVM (Support Vector Machine), a machine learning algorithm.

    Concurrently at Google, Hershey et al.\cite{vggish-paper} developed VGGish (Named after VGG\cite{simonyan2015deep}, which itself is named after the Visual Geometry Group of the University of Oxford). This was an experiment to show that \acrlong{dcnn} developed for image recognition are also excellent at recognizing sounds in spectrograms. This model was further adapted to an embedding and released to be used as such.

    Meanwhile, Arandjelovi\'c et al.\cite{l3-paper} developed L$^3$-net through a novel approach to train both a sound and video embedding: the correspondence between video and audio was used to train a network to predict whether an audio and a video segment are from the same video. This yields two sub-networks - one for video and one for audio. The audio sub-network proved to be an excellent embedding for sound recognition, almost on par with human sound recognition. While it is a much more complicated and large model, it is an excellent candidate to improve Serval's model accuracy.
