% MSc thesis style for TU Delft Embedded Networked Systems Group.

% MIT License
%
% Copyright (c) 2019 TU Delft Embedded and Networked Systems Group and Casper Dennis van Wezel.
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

\documentclass[10pt,twoside,a4paper,openright,hidelinks]{report}

% add new packages here
% \usepackage{caption}
\usepackage{subcaption}

\usepackage{adjustbox}

\usepackage{booktabs}

% math packages
\usepackage{amsmath}
\usepackage{amssymb}


% textblocks for title page
\usepackage[absolute]{textpos}

% use babel for proper hyphenation
\usepackage[british]{babel}

% Graphics: different for pdflatex or dvi output, choose one
%%\usepackage[dvips]{graphicx}
%%\usepackage[pdftex]{graphicx}
\usepackage{graphicx}

\usepackage{epstopdf}
\usepackage{rotating}
% \usepackage{subfigure}

% Make captions distinguishable
\usepackage[textfont=bf]{caption}

% Chose font style
\usepackage[scaled=.92]{helvet}

% for url's use "\url{http://www.google.com/}"
\usepackage[hyphens]{url}
% \usepackage[hyphenbreaks]{breakurl}
\usepackage[plainpages=false]{hyperref}

\usepackage{pdflscape}

\usepackage{lipsum}
\usepackage[acronym, section]{glossaries}

\makeglossaries

\input{glossary}
\input{commands}

\usepackage{emptypage}


% Information that will be filled in at various points in the report
\newcommand{\reportTitle}{Pallas: Novel Sound Classification at the Edge}
\newcommand{\reportAuthor}{Max Groenenboom} % Please put your full and official name here: no abbreviations (and to Dutch students: geen roepnamen)
\newcommand{\studentNumber}{4169298}
\newcommand{\reportEmailTUD}{m.groenenboom@student.tudelft.nl}
\newcommand{\reportEmailNonTUD}{max\_groenenboom@hotmail.com}

% Anonymize:
\renewcommand{\reportEmailTUD}{}
\renewcommand{\reportEmailNonTUD}{}

\newcommand{\reportUrlEmailTUD}{\href{mailto:\reportEmailTUD}{\reportEmailTUD}}
\newcommand{\reportUrlEmailNonTUD}{\href{mailto:\reportEmailNonTUD}{\reportEmailNonTUD}}
\newcommand{\reportMSC}{Embedded Systems}
\newcommand{\reportDate}{February, 2023}
\newcommand{\presentationDate}{February 27, 2024}
\newcommand{\graduationCommittee}{
Dr. M. A. Z\'u\~niga Zamalloa & Delft University of Technology \\
Dr. Kaitai Liang & Delft University of Technology \\
}

% Provide full name and surname (no abbreviations), so "Koen Langendoen" not "K. Langendoen"

% The order of listing above: 

% title + Graduation committee chairman name and surname (chairman)
% title + Supervisor 1 name and surname (direct supervisor)
% title + Supervisor 2 name and surname (direct supervisor)
% ...
% title + Supervisor X name and surname (direct supervisor) 
% ...
% Others (ordered by title and alphabetical)

% Example: 
% prof.\,dr.\,Koen Langendoen (chairman) & Delft University of Technology \\ 
% dr.\,Przemys{\l}aw Pawe{\l}czak & Delft University of Technology \\ 

\newcommand{\reportAbstract}{
    \note{\begin{todolist}
        \item \url{https://www.scribbr.com/dissertation/abstract/}
        \item \url{https://www.scribbr.com/academic-writing/tense-tendencies/}
        \item[\done] Introduction
        \begin{todolist}
            \item[\done] What is the problem
            \item[\done] What is the context
            \item[\done] What is the objective
            \item[\done] Should be in present or past simple tense (is or were)
        \end{todolist}
        \item[\done] Methods
        \begin{todolist}
            \item[\done] Research methods
            \item[\done] What did I do exactly, in 1 or 2 sentences
            \item[\done] Past simple tense (were)
        \end{todolist}
        \item[\done] Results
        \begin{todolist}
            \item[\done] Main results
            \item[\done] present or past simple tense (is or were)
        \end{todolist}
        \item[\done] Discussion
        \begin{todolist}
            \item[\done] Main conclusion
            \item[\done] Limitations
            \item[\done] Future work
            \item[\done] present simple tense (is)
        \end{todolist}
    \end{todolist}}
    
    Sound pollution is becoming an increasingly pressing issue in today's world. To effectively address it, it must be measured. To this end, Serval was developed, an edge-ai powered sound recognition solution. Its lack of accuracy, however, makes it difficult to deploy. This thesis examines the potential for improving this solution while staying within its technical limitations in order to raise the accuracy to satisfactory levels.

    Multiple aspects of Serval were evaluated and compared to the current state-of-the-art: its data augmentation, the embedding it uses, and the hardware it runs on. Alternatives for each of these components were evaluated and each aspect was optimized.

    The results show that after these improvements, the single-label F$_1$-score increased from 0.60 to 0.76, and the single- and multi-label combined F$_1$-score increased from 0.64 to 0.67. Finally, power consumption has been reduced by 14\%, partially thanks to the usage of specialized hardware.

    One issue that has yet to be adequately addressed is the size of the dataset. By increasing the number of samples, the accuracy could be further improved.
}

\newcommand{\reportKeywords}{
    LIST YOUR KEYWORDS HERE
}

% Did the thesis lead to a paper? 
% e.g. The work presented in this thesis has lead to a paper which has been submitted to a conference for publication, pending peer-review
%\newcommand{\reportPaper}{LINE ABOUT YOUR PUBLICATION GOES HERE}
\newcommand{\reportPaper}{}


% Information for pdflatex
\pdfinfo{
/Author (\reportAuthor)
/Title (\reportTitle)
/Keywords (\reportKeywords)
}

\usepackage[nottoc]{tocbibind}

\begin{document}

\pagenumbering{alph}
\pagestyle{empty}

% Make frontcover
\include{template/frontcover}
\todo[inline]{Fix logo}

% Set marginns
\hoffset=1.63cm
\oddsidemargin=0in
\evensidemargin=0in
\textwidth=5in

%
\parindent=1em

% Empty page
\cleardoublepage

\pagestyle{plain}
\pagenumbering{roman}
\setcounter{page}{1}

% Create title page: page i (hidden)
\include{template/titlepage}

% Create Graduation Data and Abstract: pages ii and iii (hidden)
\include{template/graduationdata}

% Empty page: page iv
\cleardoublepage

% (Optional) Include quotation: page v (uncomment if needed)
% \include{chapters/ch-0-quotation}

% Empty page: page vi
\cleardoublepage

% Start numbering here so preface is not v or vi
\pagenumbering{roman}
\setcounter{page}{1}

% Create preface: page v
\include{chapters/ch-0-preface}

% Todolist
% \listoftodos
% \listoffigures
% \listoftables

% Empty page: page vi
% \cleardoublepage

% Table of contents
\tableofcontents

\cleardoublepage

% Start page numbering
\pagenumbering{arabic}
\setcounter{page}{1}

% Introduction
\include{chapters/ch-0-introduction}

% Prior knowledge
\include{chapters/ch-1-serval}
\include{chapters/ch-1-related-work}

% Content chapters
\include{chapters/ch-2-data_augmentation}
\include{chapters/ch-2-embedding}
\include{chapters/ch-2-neural_net_on_the_edge}

% Concluding chapters
% \include{chapters/ch-3-evaluation}
\include{chapters/ch-3-conclusion}

% Bibliography
\bibliographystyle{plain} % Please do not change the style of bibliography (yes, it should be `plain`)
\bibliography{bibliography}

\clearpage

% Glossary and acronyms
\chapter*{List of terms and acronyms}
\addcontentsline{toc}{chapter}{\protect List of terms and acronyms}

\printglossary[type=\acronymtype]
\printglossary

\appendix
% Appendices
% \include{chapters/appendix_a}

\end{document}
